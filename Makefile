CC=clang  -O3
default: make_ham seek_ham test_ham

make_ham: make_ham.c hamiltonian.h
	clang make_ham.c -o make_ham -lcrypto

seek_ham: seek_ham.c hamiltonian.h
	clang seek_ham.c -o seek_ham

test_ham: test_ham.c hamiltonian.h
	clang test_ham.c -o test_ham

clean:
	rm -f *~ *.o seek_ham make_ham test_ham
