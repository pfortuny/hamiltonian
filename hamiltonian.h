#define MAXINT 0xFFFFFFFFFFFFFFFF

typedef struct graph_s {
	int vert_n, edge_n;
	int *vertex;
	int *edge;
} graph;

char *names = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ=+";


#define ERR_WRONG_USG -1
#define ERR_WRONG_SIZE 1
#define ERR_TOO_BIG    2
#define ERR_WRONG_VTX  3
#define ERR_WRONG_EDGE 4
#define ERR_TOO_LONG   5
#define ERR_SHOULD_NOT_ARRIVE -33

#define FAIL_NOT_HAM   65

/* Number of edges is v+e, (we are including a hamiltonian graph elsewhere)
 * notice that this just creates an 'empty' graph.
 * This should be an inline function but I was compiling without them
 */
#define MAKE_GRAPH(name, v, e) {\
	(name) = (graph *)calloc(1, sizeof(graph));\
	if((name)==NULL){\
		exit(-1);\
        }\
	(name)->vert_n = v;			   \
	(name)->edge_n = e;					\
	(name)->vertex = (int *) calloc(v, sizeof(int));\
	if((name)->vertex == NULL){\
		exit(-1);\
	}							\
	(name)->edge    = (int *) calloc(2*(e+v), sizeof(int ));	\
	if((name)->edge == NULL){\
		exit(-1);\
	}\
	};


int process_graph(char *, int, int );
int seek(graph *, int *, int *, int *, int *, int *);

