/* make_graph.c (c) Pedro Fortuny Ayuso, 2012
 * See LICENCE for usage (essentially unlimited).
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <openssl/rand.h>
#include <err.h>

#include "hamiltonian.h"

/* make_graph
 * Build a graph with a Hamiltonian sub-graph. The graph will
 * have a specific number of vertices and edges.
 * 
 * Input:
 * n1: number of vertices (2<n1<65)
 * n2: number of edges (an integer. n2).
 *
 * Output:
 * a string consisting of the list of edges in a graph
 * with n1 vertices and n2 edges. The string must be
 * understood as a list of 'start'-'end'. I.e.
 *
 * a.1/.a.1./
 *
 * means: a graph with the following 5 edges:
 * [a,.] [1,/], [.,a], [.,1] and [.,/]
 *
 * (vertices are implicit in the list). Edges CAN (and probably
 * will) be repeated and are oriented ([.,/] != [/,.]), but
 * there are NO 'ribbons' [a,a].
 *
 * The output is a string built from a set of 64 ASCII chars:
 * [:alnum:] + './'
 * the elements from this set are chosen randomly and the edges
 * of the graph are shuffled before being printed.
 */

char *scramble_names(){
	char *scr_names;
	scr_names = (char *)calloc(64, sizeof(char));
	memcpy(scr_names, names, 64);
	int i;
	unsigned char u,v;
        /* this is fair because our string of labels (*names)
         * is exactly 64 chars long. Reminders are evenly distributed
         * because 256 % 64 == 0. This makes the discussion in scramble_edges()
         * (see below) unnecessary in this case.
         */
	for(i=0; i<64;i++){
		RAND_bytes(&u, 1);
		u=u%64;
		v=scr_names[i];
		scr_names[i]=scr_names[u];
		scr_names[u]=v;
	}
	return scr_names;
}


void scramble_edges(graph *Gr){
	int i;
	int t[2];
	unsigned int u, max_e;

        /*
         * In order to get a fair distribution, as we are
         * computing (essentially)
         *    u = RAND % Gr->edge_n;
         * we have to make sure that the reminders are evenly
         * distributed. Assume
         *    MAXINT = K * Gr->edge_n + ro
         * where r < Gr->edge_n.
         * Then all the RANDs between
         *    K * Gr->edge_n and MAXINT
         * have to be discarded (they would give those r reminders
         * a higher chance of appearing). This is a BIT of nitpicking
         * but you have to be careful with randomness.
         */
        max_e = MAXINT - (MAXINT % Gr->edge_n);

	for(i=0; i<Gr->edge_n; i++){
        RAND_EDGE:
		RAND_bytes((unsigned char *)&u, 4);
                /* 
                 * discard those RANDs greater than
                 * max_e as per discussion above
                 */
                if(u >= max_e)
                        goto RAND_EDGE;

		u = u % Gr->edge_n;
		t[0] = Gr->edge[2*i];
		t[1] = Gr->edge[2*i+1];
		Gr->edge[2*i]=Gr->edge[2*u];
		Gr->edge[2*i+1]=Gr->edge[2*u+1];
		Gr->edge[2*u]=t[0];
		Gr->edge[2*u+1]=t[1];
	}
}


int main (int argc, char *argv[]){
        if(argc < 3 || atoi(argv[2])<atoi(argv[1]) || atoi(argv[1]) <=2){
                printf("\nUsage: %s vertices edges\n", argv[0]);
                printf("Where:\n"
                       "vertices - the number of vertices of the graph (< 65)\n"\
                       "edges    - the number of edges (> vertices).\n");
                exit(ERR_WRONG_USG);
        }
        struct timeval start;
        struct timeval end;
        int err_n;
        gettimeofday(&start, NULL);
        char *new_names;
	graph *my_graph;
	unsigned int v,e;
	v = (unsigned int) atoi(argv[1]);
	e = (unsigned int) atoi(argv[2]);
        /* macro defined in hamiltonian.h */
	MAKE_GRAPH(my_graph, v, e);

        /*
         * The way to build a random graph with a hamiltonian path
         * we follow is:
         * create the graph with no edges.
         * insert the trivial hamiltonian graph.
         * scramble the vertices (and their names).
         * scramble the edges.
         */
	int i,j;
	for(i=0;i<v;i++){
		my_graph->vertex[i]=i;
	}

	int edges=0;
	/* obvious hamiltonian path */
	for(i=0;i<v;i++){
          if((i+1)<v){ /* normal cases (all but last edge) */
            my_graph->edge[2*edges]=i;
            my_graph->edge[2*edges+1]=(i+1);
          }
          else { /* last edge */
            my_graph->edge[2*edges]=i;
            my_graph->edge[2*edges+1]=0;
          }
          edges++;
	}
        
	char *rands;
        /* get e random bytes for the orientation
         * of each edge.
         */
	rands = (char *)calloc(e, sizeof(char));
	RAND_bytes((unsigned char *)rands, e);

        /* fairness in vertices (see discussion in scramble_edges() above) */
        unsigned int max_v;
        max_v = MAXINT - (MAXINT % v);

        /* generate the remaining edges in a random way */
	while(edges<e){
		unsigned int i,j;
                /* this might be too much but endianness is
                 * tricky and we possibly want to allow
                 * > 256 vertices in the future (?).
                 */
                /* This goto loop is to ensure
                 * fairness in vertex selection, as above.
                 */
        RAND_ij:
		RAND_bytes((unsigned char *)&i, 4);
		RAND_bytes((unsigned char *)&j, 4);
                if(i >= max_v || j >= max_v)
                        goto RAND_ij;
		i = i % v;
		j = j % v;
                /* no ribbons [a,a] */
		if(i==j)
			continue;
                /* fairness in orientation */
		if(rands[edges]<128){
			my_graph->edge[2*edges]=i;
			my_graph->edge[2*edges+1]=j;
		} else {
			my_graph->edge[2*edges]=j;
			my_graph->edge[2*edges+1]=i;
		}
		edges++;
	}

        /* scramble everything */
	new_names = scramble_names();
	scramble_edges(my_graph);
	for(i=0;i<e;i++){
		printf("%c%c",
                       new_names[my_graph->edge[2*i]],
                       new_names[my_graph->edge[2*i+1]]);
	}
	printf("\n");

        /* Time should be essentially constant */
        gettimeofday(&end, NULL);
        if(end.tv_usec < start.tv_usec){
          start.tv_sec++;
          end.tv_usec+=1000000;
        }
        fprintf(stderr, 
                "time: [%i.%06i]\n",
                (int )(end.tv_sec - start.tv_sec), 
                (int )(end.tv_usec - start.tv_usec));
}


