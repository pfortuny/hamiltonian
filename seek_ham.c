/* seek_ham.c (c) Pedro Fortuny Ayuso, 2012
 * See LICENCE for usage (essentially unlimited).
 */
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <err.h>
#include <signal.h>

#include "hamiltonian.h"


/* signal handler: do not wait for more than
 * argv[2] secs.
 */

void abort_on_long(int sig){
  fprintf(stderr, "Sorry, it is taking too long\n");
  exit(ERR_TOO_LONG);
}


/* seek_ham
 * Given a graph (see make_graph.c) which supposedly contains
 * a Hamiltonian Path, search for one and report it.
 *
 * Input:
 * s: a string describing a graph, as per make_graph.c
 * t: (optional) maximum number of seconds to perform seek
 *
 * Output:
 * 
 * a string of comma-separated numbers which correspond to the
 * edges in the graph building up a hamiltonian path.
 *
 * On stderr, both the received graph and the elapsed time
 * are reported.
 */


int main(int argc, char *argv[]){
  struct timeval start;
  struct timeval end;
  int err_n;
  gettimeofday(&start, NULL);
  fprintf(stderr, "input: [%s]\n", argv[1]);
  signal(SIGALRM, abort_on_long);
  if(argc == 3){
    alarm(atoi(argv[2]));
  }      
  if((err_n = process_graph(argv[1], (int )strlen(argv[1]), 1)) < 0){
    warn("There was some error...");
    exit(err_n);
  };
  gettimeofday(&end, NULL);
  printf("\n");
  if(end.tv_usec < start.tv_usec){
    start.tv_sec++;
    end.tv_usec+=1000000;
  }
  fprintf(stderr, "time: [%i.%06i]\n", (int )(end.tv_sec - start.tv_sec), (int )(end.tv_usec - start.tv_usec));
}


/* process_graph
 *
 * Input:
 * char *edges: a string containing the graph as per
 *              make_graph.c
 * int base:    unused as of now, will specify the 'size' (in chars?) of
 *              the labels of the vertices.
 */
int process_graph(char *the_graph, int length, int base){
  
  char *vertices;
  vertices = (char *)calloc(64*64, sizeof(char));
  if(vertices == NULL){
    exit(ERR_WRONG_SIZE);
  }
  
  /* v_c: vertex counter;
   * from 1 to nr vertices:
   * we should discount later on, beware... 
   */
  int v_c=1;
  int i,j,k,l,m,n,v;

  /* this will contain the list of true 'names' of the vertices.
   * We are assuming there are at most 64 (enough for our purposes).
   */
  char *vs = (char *) calloc(65, 1);
  if(vs == NULL)
    exit(ERR_WRONG_SIZE);
  
  /*
   * Parse 'graph' (which is morally a list of edges) and get the names
   * of the vertices. 
   * 'graph' is just a long string with vertices. The fact that
   * they describe edges as pairs of vertices 
   * is irrelevant here.
   */
  /*
   * *vertices is a 'hash',
   *  vertices[i] -> what vertex number corresponds
   *                 to the vertex at position i
   * "a1bcd1"
   * vertices['a'] = 1
   * vertices['1'] = 2
   * vertices['b'] = 3
   ...
  */
  /* *vs: an array, the dual hash of vertices
   * in the example above:
   * 
   * vs[1] = 'a', vs[2] = '1', ...
   */
  for(i=0; the_graph[i] && i<length; i++){
    if(!vertices[the_graph[i]]){
      vertices[the_graph[i]] = v_c;
      vs[v_c]=the_graph[i];
      v_c++;
    }
  }
  
  /* Do build the graph as a proper structure */
  graph *Gr;
  MAKE_GRAPH(Gr, (v_c-1), (strlen(the_graph)>>1));
  /* As v_c starts in one, we need to add one
   * to i 'everywhere'
   */
  for(i=0; i<v_c-1; i++)
    Gr->vertex[i] = vs[i+1];
  /* For the same reason, now we have to
   * discount 1 from vertices[...]
   */
  for(i=0; i<strlen(the_graph); i+=2){
    Gr->edge[i] = vertices[the_graph[i]]-1;
    Gr->edge[i+1] = vertices[the_graph[i+1]]-1;
  }

  /* Get ready for the recursive call.
   * We should describe the algorithm here.
   * Not THAT difficult, but a bit messy with
   * so many conditionals.
   */
  int *V;
  int *E;
  V = (int *)calloc(Gr->vert_n, sizeof(int));
  E = (int *)calloc(Gr->vert_n, sizeof(int));
  int start;
  int end;
  unsigned int len;
  i=0;
  /*
   * The algorithm is 'simple' (the dumbest one):
   * pick an initial edge and mark its vertices as 'used'.
   * Starting from there, seek a consecutive edge which does not include
   * any of the 'used' vertices UNLESS:
   * 1) it is the last sought edge (as many as vertices) AND
   * 2) its 'free' vertex coincides with the only other 'free' vertex.
   *
   * In detail:
   * Let e=[first edge], used_v=[vertices in e], 
   * let 
   *     start_v, end_v
   * be those vertices
   * Recursively call seek(Graph, e, used_v, start_v, end_v)
   * which finds an adjacent edge, increases e, v AND updates
   * start_v and end_v (because it may be adjacent on the left or
   * on the right). This adjacent edge does not pass over an already
   * used vertex. When length[e]==vertices(Graph), we are done.
   * If this fails for the first edge, repeat with the second...
   */

  /* Notice that for simpler graphs (f.ex. graphs a lot of whose vertices
   * have incidence 2) this algorithm performs very badly because it takes
   * no advantage of the fact that, in the logging problem, there IS a
   * hamiltonian path.
   *
   * If this information is not available, then this algorithm is essentially
   * the only way to seek for a hamiltonian path, but when its existence is
   * known, one might use it and find it much faster (for simpler graphs).
   */
         
  while(i<=Gr->edge_n){          
    start = Gr->edge[2*i];
    end   = Gr->edge[2*i+1];
    /* V counts how many times have we passed over a vertex */
    V[start] = 1;
    V[end]   = 1;
    E[0] = i;
    len = 1;
    /* seek returns the length of the path, if it is the same as
     * the number of vertices, it is hamiltonian
     */
    if(seek(Gr, &start, &end, V, E, (int *)&len) == Gr->vert_n){
      goto DONE;
    }
    V[start] = 0;
    V[end]   = 0;
    E[len-1]   = 0;
    len--;
    i++;                
  }
  /* Only reachable if impossible */
  warn("There is no hamiltonian path in this graph.");
  return(FAIL_NOT_HAM);

DONE:
  for(i=0; i<Gr->vert_n; i++){
    printf("%i", E[i]);
    if(i<Gr->vert_n-1)
      printf(",");
  }
  printf("\n");
  return(0);
}

/*
 * We need a macro to prevent repeating code and a lot of
 * function calls.
 * This might have been an inline but my compiler
 * did not have them.
 *
 * JOIN simply adds an edge to the path being built,
 * and takes care of updating the endpoints and the
 * count of the vertices.
 */
#define JOIN(new_v, new_m, old_v) {             \
    if((old_v) == (new_m)){                     \
      (old_v) = (new_v);                        \
      (*len)++;                                 \
      E[(*len)-1] = i;                          \
      V[(new_v)] = 1;                           \
      new_vtx = (new_v);                        \
      goto TRY;                                 \
    }                                           \
  }


/*
 * The real thing.
 */
int seek(graph *Gr, int *start, int *end, int *V, int *E, int *len){
  int i;
  int prev_start, prev_end;
  /* debugging loop
   * for(i=0; i<*len; i++){
   *        printf("[%i,%i],",Gr->edge[2*E[i]], Gr->edge[2*E[i]+1]);
   * }
   */
  for(i=0; i<Gr->edge_n; i++){
    /* debug
     * printf("trying edge [%i]\n", i);
     * printf("[%i,%i]\n",Gr->edge[2*i], Gr->edge[2*i+1]);
     */
    int v1=Gr->edge[2*i];
    int v2=Gr->edge[2*i+1];
    int new_vtx;

    /* This is the only time both 'start' and 'end'
     * can be used: when this edge is the final one.
     * Otherwise it would not be hamiltonian.
     */
    if(*len == (Gr->vert_n-1) && 
       (((v1 == *start) && (v2 == *end))||
        ((v1 == *end) && (v2 == *start)))){
      (*len)++;
      E[*len-1] = i;
      return(Gr->vert_n);
    }

    prev_start = *start;
    prev_end   = *end;
    if((v1 == *start && v2 == *end) ||
       (v2 == *start && v1 == *end)){
      continue;
    }


    /* The following means:
     * If the hamiltonian condition is not fulfiled, try a different
     * vertex.
     * The hamiltonian condition (for a NON-TERMINAL edge) is:
     * one of the vertices is NOT in the path 
     *     AND the other is one of the endpoints of the path
     * (hence the || for the long conjunctions, and the change
     *  of roles in each line).
     */
    if(! (
         (!V[v1] && (v2 == *start || v2 == *end)) ||
         (!V[v2] && (v1 == *start || v1 == *end))
         )){
      continue;
    }

    /* The following is just ONE of the four operations
     * (join has an 'if' statement at the beginning).
     * Also, there is a goto TRY in JOIN which shortcuts
     * when the condition is fulfiled.
     * This should be read:
     * 'join the edge we are now considering at the proper endpoint
     *  of the path we are building up'
     */
    JOIN(v1, v2, *start);
    JOIN(v1, v2, *end);
    JOIN(v2, v1, *start);
    JOIN(v2, v1, *end);
  TRY:

    if(seek(Gr, start, end, V, E, len) == Gr->vert_n)
    {
      return(Gr->vert_n);
    }

    /* new_vtx DOES appear in JOIN,
     * so do not think it is a dodgy variable
     */
    V[new_vtx] = 0;
    E[*len-1] = 0;
    *end = prev_end;
    *start = prev_start;
    (*len)--;
  }

  /* unreachable: BOUM! */
  return(ERR_SHOULD_NOT_ARRIVE);
}
