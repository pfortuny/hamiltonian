/* test_ham.c (c) Pedro Fortuny Ayuso, 2012
 * This file is in the Public Domain.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>

#include "hamiltonian.h"

/* test_graph
 * 
 * Check whether a sequence of edges in a graph is a Hamiltonian
 * path.
 *
 * Input:
 * g: a graph as output by make_graph (length at most 1024 chars):
 * p: a sequence of comma-separated numbers indicating a path (each
 *       number represents the corresponding edge in the graph).
 *
 * Output:
 * a test string:
 * either FAIL!! (the sequence of edges does not correspond to a
 *       hamiltonian path). Exit status -1.
 * or the exact path as a sequence of vertices (unordered). Exit status 0.
 * 
 */

int main (int argc, char *argv[]){
  if(argc < 3){
    printf("Usage: %s graph edges\n"
           "Where: \n"
           "graph - a graph (< 1024 chars)\n"
           "edges - comma-separated sequence of vertices (nbrs)\n"
           "Example:\n"
           "%s abacadaebecdde 1,5,7,6,2\n", argv[0], argv[0]);
    exit(ERR_WRONG_USG);
  }
  char *the_graph  = argv[1];
  char *edge_seq   = argv[2];
  int graph_size   = (int ) ((float )strlen(the_graph)/2.0);
  if((graph_size) * 2 != strlen(the_graph)){
    errx(ERR_WRONG_SIZE, "Wrong size.\n");
  }

  char *path[graph_size];

  /*
   * simple limits (notice that graphs with more than 64 vertices
   * are probably too large even for moderately fast computers)
   */
  if(graph_size > 512)
    errx(ERR_WRONG_SIZE, "Wrong size: too many edges.");
  if(strlen(edge_seq) > 4096)
    errx(ERR_WRONG_SIZE, "Wrong size: path too large.");


  char *vertices = (char *)calloc(255, sizeof(char));
  int i, nr_vertices;
  nr_vertices = 0;
  /*
   * vertices is a hash: each entry (0..255) corresponds to
   * a vertex (whose label is just a char).
   * vertices[i] == 0 if charat(i) is not in the graph
   * vertices[i] == 1 if charat(i) is in the graph
   * That parsing is what the following lines do.
   */
  for(i=0; i<graph_size*2; i++){
    if(vertices[the_graph[i]] == 0){
      vertices[the_graph[i]] = 1;
      nr_vertices++;
    }
  }

  /*
   * edge_seq is a string as
   * 1,23,6,0,9
   * each number indicates an edge in the_graph[] (and following all
   * of them one supposedly has a Hamiltonian path).
   * the following code parses that string and verifies:
   *
   * 1) the numbers correspond to actual edges in the_graph[]
   * 2) the path they describe passes no more than twice over
   *    each vertex
   *
   * And keeps track of over which vertices it passes
   *
   */
  char *edge_as_char;
  int edge, path_length;
  path_length = 0;
  while((edge_as_char = strsep(&edge_seq, ",")) != NULL){
    edge = atoi(edge_as_char);
    if(edge<0 || edge > graph_size)
      errx(ERR_WRONG_VTX, "There is a bogus edge.");

    if(vertices[the_graph[2*edge]] >= 3 ||
       vertices[the_graph[2*edge+1]] >= 3)
      errx(FAIL_NOT_HAM, "This path passes more than once over a vertex.");

    vertices[the_graph[2*edge]]++;
    vertices[the_graph[2*edge+1]]++;
    path_length++;
  }


  /* 
   * A path is hamiltonian if
   *
   * 1) its length is exactly the number of vertices in the_graph[]
   * 2) it passes exactly once over each vertex
   *    (the test includes == 0 because vertices[] is actually
   *     a hash and non-existent vertices will have a value of 0)
   *
   */
  if(path_length != nr_vertices){
    warn("The path is too short");
    exit(5);
  }
  for(i=0; i<nr_vertices; i++)
    if(vertices[i] != 3 && vertices[i] != 0){
      warn("There is an omitted vertex");
      exit(5);
    }

  fprintf(stderr, "The path is hamiltonian.\n");
  exit(0);

}
